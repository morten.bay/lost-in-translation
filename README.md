# Lost-In-Translation

An online sign language translator as a Single Page Application using the React framework.

## Description

The application features 3 main components:

* A Startup page - where you can "login on" to the application with a username of choice.

* A Translation page - where you can translate words and sentences from regular text to sign language.

* A Profile page - where you can view your translation history and clear them if you wish.

## Installation

Dependencies: https://github.com/dewald-els/noroff-assignment-api

Recommended runtime: [Node.js - LTS version](https://nodejs.org)

Install the project
```
1. git clone git@gitlab.com:morten.bay/lost-in-translation.git
2. npm install
```
Create environment variables
```
3. create a ".env" file in the root of the project folder and add the key and url for the noroff-assigment-api:

REACT_APP_API_KEY=
REACT_APP_API_URL=
```

## Usage
```
1. npm start
2. Browse to http://localhost:3000
```
## Contributors

* [NKE @nicolai_eng](@nicolai_eng)
* [MBN @morten.bay](@morten.bay)

## License
[MIT](https://choosealicense.com/licenses/mit/)
