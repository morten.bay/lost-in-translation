import withAuth from "../hoc/withAuth"
import TranslationForm from "../components/Translation/TranslationForm"

const Translation = () => {
    return (
        <>
            <TranslationForm/>
        </>
    )
}
export default withAuth(Translation)