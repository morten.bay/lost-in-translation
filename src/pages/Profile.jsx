import ProfileActions from "../components/Profile/ProfileActions"
import ProfileHeader from "../components/Profile/ProfileHeader"
import ProfileTranslationHistory from "../components/Profile/ProfileTranslationHistory"
import { useUser } from "../contexts/UserContext"
import withAuth from "../hoc/withAuth"

const Profile = () => {

    const { user } = useUser() // Setup user context 

    return (
        <>     
            <ProfileHeader username={user.username} />
            <ProfileTranslationHistory translations={user.translations} />
            <ProfileActions/>
        </>

    )
}
// WithAuth wrapper function ensures that a redirect will happen if the user is not logged in.
export default withAuth(Profile)