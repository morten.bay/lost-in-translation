// Features
import { useState } from "react" 
import { useForm } from "react-hook-form"
// Imported function
import { dataToApi, fetchTrans } from "../../api/translate"
//Styling
import "./TranslationForm.css"
import { useUser } from "../../contexts/UserContext"

import { storageSave } from "../../utils/storage"
import { STORAGE_KEY_USER } from "../../const/storageKeys"

// validates user input according to criteria.
const transTextConfig = {
    required:true, // empty fields are not allowed.
    maxLength: 40, // sets minimum letters to 2
    pattern: /^[a-zA-Z ]+$/g // Only letters
}

const TranslationForm = () => {
    // Include from useForm:
    const {
        register,
        handleSubmit, 
        formState: {errors}
    } = useForm()

    const {user, setUser} = useUser()
    const [transWord, setTransWord] = useState([]) // Word for translation

    // Called for submit event
    const onSubmit = async (text) =>{
        setTransWord(text.transText.replace(/\s/g,"").toLowerCase()) // Updates word for translation
        const fetchTransError = await dataToApi(text.transText, user.id) // Updates translation history with new word

        // When the transactions has been updated, fetch it from the api and set the user context and local state to reflect the changes.
        const [setHistoryError, Response] = await fetchTrans(user.id)
        if(fetchTransError !== null || setHistoryError !== null) {
            throw new Error("Api error")
        }
        if(Response !== null) {

            const updatedUser = {
                ...user,
                translations: Response
            }
            storageSave(STORAGE_KEY_USER, updatedUser)
            setUser(updatedUser)
        }
    }

    // Creates list of hand signs
    const picList = (() => {
        let list1 = [];
        // Loops through each letter in user-entered word, and acquires associated image.
        for(let i = 0; i < transWord.length; i++) {
                list1.push(<img key = {`${i}`} 
                src = {require(`${process.env.PUBLIC_URL}/public/resources/individial_signs/${transWord[i]}.png`)} 
                className = "imgHands"
                alt = {`${i}`}/>)
        }
        return list1
    })()

    // Writes error if user-text doesn't live up to validation criteria. 
    const errorMessage = (() => {
        if (!errors.transText){ // Breaks out if no error
            return null
        }
        if (errors.transText.type === "required"){  // Error for no entry.
            return <p className="SubmitTextTrans"> Text input is required </p>
        }
        if (errors.transText.type === "pattern"){  // Error for non-letter entry.
            return <p className="SubmitTextTrans"> Only letter inputs </p>
        }
        if (errors.transText.type === "maxLength") { // Error for exceeding max length
            return <p className="SubmitTextTrans"> Translation is to long (max 40 letters) </p>
        }
    })()

    return (
        <> 
            <img className="TransLogo" src="./resources/Logo.png" alt="LOGO"></img> 
            <h1 className="TransTitle"> Translator </h1>
            <form onSubmit={handleSubmit(onSubmit)}> {/* OnSubmit only triggers if validation is successful */}
                <fieldset className="TransField">
                    <label className ="TransButtonLabel" htmlFor = "transText"> Text: </label>
                    <input className ="FieldTrans" type="text"
                    placeholder="Enter word" // Written for no entry
                    {...register("transText", transTextConfig)} /> {/*Checks if username lives up to validation criteria */}
                    <button className="TransButton" type = "submit">Continue</button> {/* Triggers submit event */}
                </fieldset>
                { errorMessage } {/* Calls error function*/}
            </form>
            <div className = "TransHandBox">
                { picList } {/* List of images */}
            </div>
        </>
    )
}
export default TranslationForm