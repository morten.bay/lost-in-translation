import { NavLink } from "react-router-dom"
import { useUser } from "../../contexts/UserContext"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { storageDelete } from "../../utils/storage"
import "../../App.css"

const Navbar = ({ logout }) => {

    // Setup the user context 
    const { user, setUser } = useUser()

    // Handler for logoutClick logic
    const handleLogoutClick = () => {
        if(window.confirm("Are you sure?"))
        {
            // Delete the stored login data
            // Without() automatically redirects the user to the login page.
            storageDelete(STORAGE_KEY_USER)
            setUser(null)
        }
    }

    return (
        <nav className="fullWidth">
            { user !== null &&
                <div className="navBar">
                    <div className="navItem"><NavLink className = "navLink" to="/profile">Profile</NavLink></div>
                    <div className="navItem"><NavLink className = "navLink"to="/translation">Translations</NavLink></div>
                    <div className="navItem" onClick={ handleLogoutClick }>
                    <NavLink className = "navLink" to="/translation">Logout</NavLink></div>
                </div>
            }
        </nav>
    )
}

export default Navbar