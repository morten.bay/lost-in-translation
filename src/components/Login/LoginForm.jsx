import { useState, useEffect } from "react"
import {useForm} from "react-hook-form"
import { useNavigate } from "react-router-dom"
import { loginUser } from "../../api/translate"
import { useUser } from "../../contexts/UserContext"
import { storageSave } from "../../utils/storage"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import "./LoginForm.css"

// Setup login form validation requirements.
const usernameConfig = {
    required: true,
    minLength: 3
}

const LoginForm = () => {

    // Setup form hook to enable validation
    const { register, handleSubmit, formState : { errors } } = useForm() 

    // Setup the user context 
    const { user, setUser} = useUser() 

    // Local States
    const [ loading, setLoading ] = useState(false)
    const [ apiError, setApiError ] = useState(null)

    // Setup Navigate component
    const navigate = useNavigate()

    // useEffect is used to redirect a already logged in user to the translation page
    // since the login page is public and the other pages is handled using withAuth()
    useEffect(() => {

        // Check if the user is logged in, then redirect to translation page
        if (user !== null) {
            navigate("translation")
        }
    }, [ user, navigate])

    // onSubmit triggers the login logic, if successful the user context and local storage is updated.
    const onSubmit = async ({ username }) => {
        setLoading(true)
        const [error, userResponse] = await loginUser(username)
        if(error) {
            setApiError(error)
            return
        }
        if(userResponse !== null) {
            storageSave(STORAGE_KEY_USER, userResponse)
            setUser(userResponse)
        }
        setLoading(false)
    }

    // Display form validation errors and check that the .env keys are set.
    const errorMessage = (() => {

        if(!process.env.REACT_APP_API_URL || !process.env.REACT_APP_API_KEY)
        {
            return <span>Error .env keys not set!</span>
        }

        if(!errors.username) {
            return null;
        }

        if(errors.username.type === 'required') {
            return <p className = "SubmitTextLogin"> Please enter a Username</p>
        } 

        if(errors.username.type === 'minLength') {
            return <p className = "SubmitTextLogin"> Username is too short, must be 3 characters minimum.</p>
        }
    })() 

    // Login Form
    return (
        <div className ="FormLogin">
            <h1 className = "StartTitle"> Welcome to TranslatoBot 2000!</h1>
            <p className = "SloganLogin"> Possibly the worlds best sign-language translator using emojis!</p>
            <div className ="BigBoxLogin">
            <div className="LogoHelloBG">
            <img className="LogoHello" src="./resources/Logo-Hello.png" alt="LOGO"></img>
            </div>
                <form onSubmit={ handleSubmit(onSubmit)}>
                    <fieldset className="FieldLogin">
                        <label className="ButtonTextLogin" htmlFor="username">Username: </label>
                        <input type="text" className = "LoginField" placeholder="Hello" {...register("username", usernameConfig)}  />
                        <button type="submit"className = "LoginButton" disabled={ loading }>Login</button>
                    </fieldset>
                    {errorMessage}
                    { loading && <p className = "SubmitLoadingLogin">Logging in...</p> }
                    { apiError && <p className = "SubmitTextLogin">{apiError}</p> }
                </form>
            </div>
        </div>
    )
}
export default LoginForm