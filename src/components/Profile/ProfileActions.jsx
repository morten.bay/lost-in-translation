import { useState } from "react"
import { clearTranslationHistory } from "../../api/translate"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { useUser } from "../../contexts/UserContext"
import { storageSave } from "../../utils/storage"
import "./Profile.css"

const ProfileActions = () => {

    // Setup the user context 
    const {user, setUser} = useUser()

    // Local States
    const [ apiError, setApiError ] = useState(null)

    // handler for the ClearHistoryClick logic.
    const handleClearHistoryClick = async () => {
        if(window.confirm("Are you sure?"))
        {
            // Call api to clear the Translation history.
            const [clearError] = await clearTranslationHistory(user.id)
            
            if(clearError) // If an error is found display the error message and return.
            {
                setApiError(clearError)
                return
            }

            const updatedUser = {
                ...user,
                translations: []
            }

            // Save the user context and local storage to reflect the changes in the API.
            storageSave(STORAGE_KEY_USER, updatedUser)
            setUser(updatedUser)
            }
    }

    return (
        <>
            <button className="clearButton" onClick={ handleClearHistoryClick }>Clear history</button>
            { apiError && <p>{apiError}</p> }
        </>
    )

}
export default ProfileActions