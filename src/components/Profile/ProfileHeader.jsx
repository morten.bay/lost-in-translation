const ProfileHeader = ( {username}) => {

    return (
        <header>
            <img className="imgProfile" src="./resources/user-icon.png" alt="LOGO"></img>  
            <h3 className="userText">Username: {username} </h3>
        </header>
    )
}
export default ProfileHeader