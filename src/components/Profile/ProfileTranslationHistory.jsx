import ProfileTranslationHistoryItem from "./ProfileTranslationHistoryItem"

const ProfileTranslationHistory = ( {translations}) => {

    // Displays each translation value from the translation history.
    const translationList = translations.map(
        (translation, index) => <ProfileTranslationHistoryItem key={ index + '-' + translation } translations={ translation }/>)

    return (
        <section>
            <div className="historyList">
            <p className= "historyText">Translation history:</p>
                <ul>
                    { translationList }
                </ul>
            </div>
        </section>
    )
}
export default ProfileTranslationHistory