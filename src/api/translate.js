import { createHeaders } from "./headers"// import guide patching specification for API update.

const apiUrl = process.env.REACT_APP_API_URL // API url (to heroku API)

// Collects previous translations and updates API with new ones.
export const dataToApi = async (textTrans, user) => {
    const [checkError, translations] = await fetchTrans(user) // Fetches translation data from heroki API. 
    // Stops the update if a fetching error occurred
    if (checkError !== null) {
        return [checkError, null]
    }

    // Checks if 10 history elements are already present
    if (translations.length >= 10){
        translations.shift() // removes oldest element
    } 
    
    translations.push(textTrans) // adds new element

    return await updateApi(translations, user) // creates new user and returns to onSubmit function.
}

// Fetches data translation data from Heroku API
export const fetchTrans = async (userID) => { // 
    try { 
        const response = await fetch(`${apiUrl}/${userID}`) // Fetches data
        if (!response.ok){ // Checks if successful fetch
            throw new Error("Could not complete request")
        }
        // converts API data to json and returns.
        const data = await response.json() 
        return [null, data.translations]
    }
    // Sends error if fetch is not successful
    catch (error) {
        return[error.message, []]
    }
}

// Updates Heroku API with newest translation
export const updateApi = async (translation,userID) => {
    try {
        await fetch(`${apiUrl}/${userID}`, { // Accesses data for logged in user
            method: "PATCH", // Only update a single record
            headers: createHeaders(),
            body: JSON.stringify({ // Data for API update
                translations: translation
            })
        })
    }
    // Returns error if update was not successful.
    catch (error) {
        return error.message
    }
    return null // no error
}  

// Checks if provided username already exists in the API
export const checkForUser = async (username) => {
    try {
        const response = await fetch(`${apiUrl}?username=${username}`)
        if(!response.ok) {
            throw new Error("Could not complete request")
        }
        const data = await response.json()
        return [ null, data ]

    } catch (error) {
        return [ error.message, [] ]
    }
}

// Creates a new user using POST method
const createUser = async (username) => {
    try {
        const response = await fetch(`${apiUrl}`, {
            method: 'POST',
            headers: createHeaders(),
            body: JSON.stringify({
                username,
                translations: []
            })
        })
        if(!response.ok) {
            throw new Error(`Could not create user with username: ${username}`)
        }
        const data = await response.json()
        return [ null, data ]

    } catch (error) {
        return [ error.message, [] ]
    }
}

// Login function first checks for an existing user. 
// If an existing user is found, that one is returned, otherwise a new user is created and returned.
export const loginUser = async (username) => {
    try {
        const [checkError, user] = await checkForUser(username)

        if(checkError) {
            return [ checkError, null ]
        }
        if (user.length > 0) {
            return [ null, user.pop() ]
        }

        return await createUser(username)

    } catch (error) {
        return [error.message, null]
    }
}

// Clears a users translation history using PATCH method.
export const clearTranslationHistory = async (userId,) => {
    try {
        const response = await fetch(`${apiUrl}/${userId}`, {
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations: [] // translations are set to an empty array
            })
        })
        if(!response.ok){
            throw new Error("Could not delete history")
        }
        const result = await response.json()
        return [null, result]
    } catch (error) {
        return [error.message, null]
    }
}